package loicduparc.myapp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import loicduparc.myapp.enums.LayoutType;
import loicduparc.myapp.listener.SectionListener;
import loicduparc.myapp.models.DataSetEvent;
import loicduparc.myapp.models.SearchEvent;
import loicduparc.myapp.models.Section;
import loicduparc.myapp.service.MyService;
import loicduparc.myapp.utils.Utils;

public class MainActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    protected final String ARG_SELECTED_LAYOUT_ID = TAG + ":selectedLayoutId";
    protected final String ARG_CACHED_DATA = TAG + ":data";
    protected final String ARG_BINDED_SERVICE = TAG + ":serviceBinded";

    private final LayoutType DEFAULT_LAYOUT = LayoutType.LIST;
    private String mSelectedLayoutType;
    private SearchView mSearchView;
    private MainFragment mFragment;
    private IService mEndpoint;
    private boolean mIsBound = false;

    private ArrayList<Section> mCachedData;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LifeCycle
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        this.mToolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (savedInstanceState != null) {
            this.mSelectedLayoutType = savedInstanceState.getString(ARG_SELECTED_LAYOUT_ID, DEFAULT_LAYOUT.name());
            this.mCachedData = savedInstanceState.getParcelableArrayList(ARG_CACHED_DATA);
            this.mIsBound = savedInstanceState.getBoolean(ARG_BINDED_SERVICE, false);
        } else {
            this.mSelectedLayoutType = Utils.isTablet(this) ? LayoutType.GRID.name() : DEFAULT_LAYOUT.name();
            this.mCachedData = new ArrayList<>();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        this.mFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(TAG);
        if (this.mFragment == null) {
            this.mFragment = MainFragment.newInstance(mSelectedLayoutType, mCachedData);
            fragmentTransaction.add(R.id.container, this.mFragment, TAG).commit();
        } else {
            fragmentTransaction.attach(this.mFragment).commit();
        }

        startService();

    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService();
        if (mFragment != null) {
            getSupportFragmentManager().beginTransaction().detach(mFragment).commit();
        }
    }

    @Override
    protected void onDestroy() {
        stopService();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_SELECTED_LAYOUT_ID, this.mSelectedLayoutType);
        outState.putParcelableArrayList(ARG_CACHED_DATA, mCachedData != null ? this.mCachedData : new ArrayList<Section>());
        outState.putBoolean(ARG_BINDED_SERVICE, this.mIsBound);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Menu
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        this.mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        this.mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        this.mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == R.id.action_search) {
            this.mSearchView.setIconified(false);
            return true;
        }
        return false;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SearchView.OnQueryTextListener
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public boolean onQueryTextSubmit(String constraint) {
        EventBus.getDefault().post(new SearchEvent(constraint));
        return true;
    }

    @Override
    public boolean onQueryTextChange(String constraint) {
        EventBus.getDefault().post(new SearchEvent(constraint));
        return false;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SERVICE
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void startService() {
        if (!this.mIsBound) {
            Log.i(TAG, "startService");
            Intent serviceIntent = new Intent(MyService.class.getName());
            serviceIntent.setPackage("loicduparc.myapp");
            startService(serviceIntent);

            bindService(serviceIntent, this.mServiceConnection, BIND_AUTO_CREATE);
            this.mIsBound = true;
        }
    }

    private void stopService() {
        try {
            Log.i(TAG, "stopService");
            if (mEndpoint != null) {
                mEndpoint.removeListener(mServiceListener);
            }
            stopService(new Intent(MyService.class.getName()));
        } catch (Throwable ignore) {

        }
    }

    private void unbindService() {
        if (this.mIsBound) {
            unbindService(mServiceConnection);
            this.mIsBound = false;
        }

    }

    private SectionListener.Stub mServiceListener = new SectionListener.Stub() {
        @Override
        public void handleSectionProcessed(List<Section> sections) throws RemoteException {
            mCachedData.clear();
            mCachedData.addAll(sections);

            EventBus.getDefault().post(new DataSetEvent(sections));
        }

        @Override
        public void handlehttpError(final String error) throws RemoteException {
            Log.e(TAG, "handlehttpError :["+error+"]");
            Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "Service connection established");

            mEndpoint = IService.Stub.asInterface(service);
            try {
                mEndpoint.addListener(mServiceListener);
                mEndpoint.getAllWebsites();
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to add listener", e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mEndpoint = null;
            Log.i(TAG, "Service connection closed");
        }
    };


}
