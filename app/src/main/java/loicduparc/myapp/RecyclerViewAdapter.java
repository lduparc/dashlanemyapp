package loicduparc.myapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import de.greenrobot.event.EventBus;
import loicduparc.myapp.enums.ItemType;
import loicduparc.myapp.models.ItemClickEvent;
import loicduparc.myapp.models.Section;
import loicduparc.myapp.utils.Utils;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable, IItemClickListener {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_EMPTY = 2;
    private static final int MIN_ITEMS_COUNT = 1;

    private final Context mContext;
    private final List<Section> mOriginalItems;
    private final Interpolator mInterpolator = new DecelerateInterpolator();
    private List<Section> mFilteredItems;
    private ItemFilter mFilter;

    public RecyclerViewAdapter(Context context, List<Section> dataSet) {
        this.mContext = context;

        this.mOriginalItems = dataSet;
        this.mFilteredItems = dataSet;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (TYPE_HEADER == viewType) {
            final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_header, parent, false);
            ViewGroup.LayoutParams layoutManager = view.getLayoutParams();
            if (layoutManager instanceof StaggeredGridLayoutManager.LayoutParams) {
                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) layoutManager;
                layoutParams.setFullSpan(true);
                view.setLayoutParams(layoutParams);
            }
            return new HeaderViewHolder(view);
        } else if (TYPE_EMPTY == viewType) {
            final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_empty, parent, false);
            return new EmptyViewHolder(view);
        } else if (TYPE_ITEM == viewType) {
            final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item, parent, false);
            return new ItemViewHolder(view, this);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (TYPE_HEADER == viewType) {
            bindHeaderView((HeaderViewHolder) holder, position);
        } else if (TYPE_EMPTY == viewType) {
            bindEmptyView((EmptyViewHolder) holder);
        } else if (TYPE_ITEM == viewType) {
            bindItemView((ItemViewHolder) holder, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (this.mFilteredItems.size() == 0) {
            return TYPE_EMPTY;
        }

        return this.mFilteredItems.get(position).getType() == ItemType.ITEM ? TYPE_ITEM : TYPE_HEADER;
    }

    @Override
    public int getItemCount() {
        int count = this.mFilteredItems.size();
        return count > 0 ? this.mFilteredItems.size() : MIN_ITEMS_COUNT;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * IItemClickListener
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public void showItemAtPosition(View view, int position) {
        if (position < this.mFilteredItems.size()) {
            EventBus.getDefault().post(new ItemClickEvent(view, this.mFilteredItems.get(position)));
        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * View Binder
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void bindHeaderView(final HeaderViewHolder holder, int position) {
        Section item = this.mFilteredItems.get(position);
        holder.name.setText(item.getName());
    }

    private void bindEmptyView(final EmptyViewHolder holder) {
        //Nothing to do !
    }

    private void bindItemView(final ItemViewHolder holder, int position) {
        Section item = this.mFilteredItems.get(position);
        holder.name.setText(item.getName());
        if (!TextUtils.isEmpty(item.getIcon())) {
            if (holder.subname != null) {
                holder.subname.setText(item.getIcon());
            }
            Picasso.with(this.mContext)
                    .load(Utils.IMAGE_URL + item.getIcon())
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(holder.icon);
        } else if (holder.subname != null) {
            holder.subname.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Filterable
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ItemFilter();
        }
        return mFilter;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * View Holder
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    static class HeaderViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.name)
        TextView name;

        public HeaderViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    static class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View view) {
            super(view);
        }
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.icon)
        ImageView icon;
        @InjectView(R.id.name)
        TextView name;
        @Optional
        @InjectView(R.id.subname)
        TextView subname;
        private IItemClickListener listener;

        public ItemViewHolder(View view, IItemClickListener listener) {
            super(view);
            ButterKnife.inject(this, view);
            this.listener = listener;
        }

        @OnClick(R.id.container)
        void onItemClick() {
            if (this.listener != null) {
                this.listener.showItemAtPosition(icon, getPosition());
            }
        }

    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (TextUtils.isEmpty(constraint)) {
                results.values = RecyclerViewAdapter.this.mOriginalItems;
                results.count = RecyclerViewAdapter.this.mOriginalItems.size();
                return results;
            }

            String filterString = constraint.toString().toLowerCase();

            final List<Section> list = RecyclerViewAdapter.this.mOriginalItems;

            int count = list.size();
            final ArrayList<Section> nlist = new ArrayList<>(count);

            Section filterableSection;

            for (int i = 0; i < count; i++) {
                filterableSection = list.get(i);
                if (filterableSection.getType() == ItemType.ITEM && filterableSection.getName().toLowerCase().contains(filterString.toLowerCase())) {
                    nlist.add(filterableSection);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            RecyclerViewAdapter.this.mFilteredItems = (ArrayList<Section>) results.values;
            notifyDataSetChanged();
        }

    }
}
