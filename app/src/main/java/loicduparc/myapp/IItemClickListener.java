package loicduparc.myapp;

import android.view.View;

/**
 * Created by loicduparc on 29/04/15.
 */
public interface IItemClickListener {

    void showItemAtPosition(View view, int position);
}
