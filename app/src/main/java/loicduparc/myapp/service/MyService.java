package loicduparc.myapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import loicduparc.myapp.IService;
import loicduparc.myapp.enums.ItemType;
import loicduparc.myapp.listener.SectionListener;
import loicduparc.myapp.models.Section;
import loicduparc.myapp.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;

public class MyService extends Service {

    private static final String TAG = MyService.class.getSimpleName();
    private DashlaneService mWebService;
    private final List<SectionListener> mListeners = new ArrayList<>();

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LifeCycle
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (MyService.class.getName().equals(intent.getAction())) {
            if (mWebService == null) {
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setConverter(new SectionConverter())
                        .setEndpoint(Utils.SERVER_URL).build();
                mWebService = restAdapter.create(DashlaneService.class);
            }
            return endpoint;
        }

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Interfaces
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private IService.Stub endpoint = new IService.Stub() {

        @Override
        public void getAllWebsites() throws RemoteException {

            synchronized (mListeners) {

                if (mWebService == null) {
                    throw new RemoteException("WebService interface cannot be null");
                }

                mWebService.listWebSites(webserviceCallback);
            }
        }

        @Override
        public void addListener(SectionListener listener)
                throws RemoteException {

            synchronized (mListeners) {
                mListeners.add(listener);
            }
        }

        @Override
        public void removeListener(SectionListener listener)
                throws RemoteException {

            synchronized (mListeners) {
                mListeners.remove(listener);
            }
        }

    };

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * REST
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private Callback<List<Section>> webserviceCallback = new Callback<List<Section>>() {
        @Override
        public void success(List<Section> sections, Response response) {

            synchronized (mListeners) {
                for (SectionListener listener : mListeners) {
                    try {
                        if (sections == null) {
                             failure(RetrofitError.unexpectedError(Utils.SERVER_URL, new Throwable("SectionConverter Error")));
                        } else {
                            listener.handleSectionProcessed(sections);
                        }
                    } catch (RemoteException e) {
                        Log.w(TAG, "Failed to notify listener " + listener, e);
                    }
                }
            }
        }

        @Override
        public void failure(RetrofitError error) {
            synchronized (mListeners) {
                for (SectionListener listener : mListeners) {
                    try {
                        listener.handlehttpError(error.getMessage());
                    } catch (RemoteException e) {
                        Log.w(TAG, "Failed to notify listener " + listener, e);
                    }
                }
            }
        }
    };

    static class SectionConverter implements Converter {

        @Override
        public List<Section> fromBody(TypedInput typedInput, Type type) throws ConversionException {
            List<Section> sections = new ArrayList<>();
            try {
                sections.addAll(fromStream(typedInput.in()));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            return sections;
        }

        @Override
        public TypedOutput toBody(Object o) {
            return null;
        }

        public static List<Section> fromStream(InputStream in) throws IOException {
            final List<Section> sections = new ArrayList<>();
            BufferedReader buf = null;
            Character lastSectionIndex = null;
            String lastWebsiteAdded = null;
            String wesite;
            try {
                buf = new BufferedReader(new InputStreamReader(in));
                while ((wesite = buf.readLine()) != null) {
                    char currentChar = wesite.toLowerCase().charAt(0);
                    if (lastSectionIndex == null || lastSectionIndex != currentChar) {
                        Log.d(TAG, "add Header : " + ("" + currentChar));
                        sections.add(new Section("" + currentChar, null, ItemType.HEADER));
                    }

                    String name = wesite.replaceAll("(@|\\.)\\w+", "").replace('_', '.');
                    if (lastWebsiteAdded == null || !name.equalsIgnoreCase(lastWebsiteAdded)) {
                        Log.d(TAG, "add Item : " + name);
                        sections.add(new Section(name, wesite));
                    }
                    lastSectionIndex = currentChar;
                    lastWebsiteAdded = name;
                }
            } catch (IOException e) {
               throw new IOException(e.getCause());
            } finally {
                if (buf != null) {
                    try {
                        buf.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sections;
        }
    }
}
