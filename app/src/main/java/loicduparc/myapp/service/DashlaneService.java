package loicduparc.myapp.service;

import java.util.List;

import loicduparc.myapp.models.Section;
import retrofit.Callback;
import retrofit.http.GET;

public interface DashlaneService {
    @GET("/listing.txt")
    void listWebSites(Callback<List<Section>> callback);
}
