package loicduparc.myapp.utils;

import android.content.Context;
import android.content.res.Configuration;

/**
 * Created by loicduparc on 28/04/15.
 */
public class Utils {

    public static final String SERVER_URL = "https://dl.dropboxusercontent.com/u/2532281";
    public static final String IMAGE_URL = "https://s3-eu-west-1.amazonaws.com/static-icons/_android48/";

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }
}
