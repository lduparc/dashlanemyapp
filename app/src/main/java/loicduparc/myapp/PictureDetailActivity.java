package loicduparc.myapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import loicduparc.myapp.models.ItemClickEvent;
import loicduparc.myapp.models.Section;
import loicduparc.myapp.utils.Utils;

public class PictureDetailActivity extends ActionBarActivity {

    private static final String TAG = PictureDetailActivity.class.getSimpleName();
    private static final String EXTRA_IMAGE = TAG + ":image";
    private static final String EXTRA_TITLE = TAG + ":title";

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    @InjectView(R.id.image)
    ImageView mImage;


    public static void launch(Activity activity, ItemClickEvent event) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, event.getView(), EXTRA_IMAGE);

        Section section = event.getSection();

        Intent intent = new Intent(activity, PictureDetailActivity.class);
        intent.putExtra(EXTRA_TITLE, section.getName());
        intent.putExtra(EXTRA_IMAGE, section.getIcon());
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_detail);
        ButterKnife.inject(this);

        Bundle data = getIntent().getExtras();
        mToolbar.setTitle(data.getString(EXTRA_TITLE));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ViewCompat.setTransitionName(mImage, EXTRA_IMAGE);

        String path = Utils.IMAGE_URL + data.getString(EXTRA_IMAGE);
        Picasso.with(this).load(path).into(mImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}