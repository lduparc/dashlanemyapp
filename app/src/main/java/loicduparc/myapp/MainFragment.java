package loicduparc.myapp;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import loicduparc.myapp.enums.LayoutType;
import loicduparc.myapp.models.DataSetEvent;
import loicduparc.myapp.models.ItemClickEvent;
import loicduparc.myapp.models.SearchEvent;
import loicduparc.myapp.models.Section;
import loicduparc.myapp.utils.Utils;
import loicduparc.myapp.views.CustomDialog;

public class MainFragment extends Fragment {

    private static final String TAG = MainFragment.class.getSimpleName();
    private static final String ARG_LAYOUT_TYPE = TAG + ":layout_id";
    private static final String ARG_DATA = TAG + ":data";

    @InjectView(R.id.recycler)
    RecyclerView mRecyclerView;

    private CustomDialog mCustomDialog;
    private LayoutType mLayoutType;
    private ArrayList<Section> mDataSet;

    public MainFragment() {
    }

    public static MainFragment newInstance(String layoutType, ArrayList<Section> dataSet) {
        MainFragment fragment = new MainFragment();

        Bundle args = new Bundle();
        args.putString(ARG_LAYOUT_TYPE, layoutType);
        args.putParcelableArrayList(ARG_DATA, dataSet != null ? dataSet : new ArrayList<Section>());
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LifeCycle
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutType = LayoutType.valueOf(getArguments().getString(ARG_LAYOUT_TYPE));
        mDataSet = getArguments().getParcelableArrayList(ARG_DATA);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Context context = getActivity();

        mRecyclerView.setLayoutManager(createLayoutManager(context));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(new RecyclerViewAdapter(context, this.mDataSet));
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //TODO : manage scroll in order to avoid IllegalStateException on notifyDataSetChanged
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LayoutManager Helper
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private RecyclerView.LayoutManager createLayoutManager(Context context) {
        if (mLayoutType == LayoutType.LIST) {
            return new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        }

        int display_mode = getResources().getConfiguration().orientation;
        return new StaggeredGridLayoutManager(display_mode == Configuration.ORIENTATION_PORTRAIT ? 2 : 4, StaggeredGridLayoutManager.VERTICAL);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Event
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public void onEvent(SearchEvent event) {
        if (mRecyclerView != null && mRecyclerView.getAdapter() != null && mRecyclerView.getAdapter() instanceof Filterable) {
            ((Filterable) mRecyclerView.getAdapter()).getFilter().filter(event.getConstraint());
        }
    }
    public void onEventMainThread(DataSetEvent event) {
        if (mRecyclerView != null && mRecyclerView.getAdapter() != null) {
            mDataSet.clear();
            mDataSet.addAll(event.getSections());
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    public void onEvent(ItemClickEvent event) {
        final Activity activity = getActivity();
        if (activity != null && !activity.isFinishing()) {
            if (Utils.isTablet(activity)) {
                showDialog(activity, event);
            } else {
                PictureDetailActivity.launch(activity, event);
            }
        }
    }

    private void showDialog(final Activity activity, ItemClickEvent event) {
        final Section section = event.getSection();

        if (mCustomDialog == null) {
            mCustomDialog = new CustomDialog.Builder(activity, false)
                .setPositiveButton(getString(android.R.string.ok), Color.WHITE, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCustomDialog.dismiss();
                    }
                }).setNegativeButtonVisibility(View.GONE)
                .prepare();

        }
        mCustomDialog.setTitle(section.getName());

        Picasso.with(activity).load(Utils.IMAGE_URL + section.getIcon()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mCustomDialog.setIcon(new BitmapDrawable(getResources(), bitmap));
                mCustomDialog.show();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                mCustomDialog.setIcon(R.mipmap.ic_launcher);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }
}
