package loicduparc.myapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by loicduparc on 28/04/15.
 */
public class DataSetEvent {

    private List<Section> sections;

    public DataSetEvent(List<Section> sections) {
        this.sections = sections;
    }

    public List<Section> getSections() {
        return sections == null ? new ArrayList<Section>() : sections;
    }
}
