package loicduparc.myapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import loicduparc.myapp.enums.ItemType;

/**
 * Created by loicduparc on 28/04/15.
 */
public class Section implements Parcelable {

    private String name;
    private String icon;
    private ItemType type;

    public Section(String name, String icon) {
        this(name, icon, ItemType.ITEM);
    }

    public Section(String name, String icon, ItemType type) {
        this.name = name;
        this.icon = icon;
        this.type = type;
    }

    public Section(Parcel source) {
        this.name = source.readString();
        this.type = ItemType.valueOf(source.readString());

        if (this.type == ItemType.ITEM) {
            this.icon = source.readString();
        }
    }

    public String getName() {
        return name;
    }

    public Section setName(String name) {
        this.name = name;
        return this;
    }

    public String getIcon() {
        return icon;
    }

    public Section setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public ItemType getType() {
        return type;
    }

    public Section setType(ItemType type) {
        this.type = type;
        return this;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<Section> CREATOR = new Creator<Section>() {
        @Override
        public Section createFromParcel(final Parcel source) {
            return new Section(source);
        }

        @Override
        public Section[] newArray(final int size) {
            return new Section[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.name);
        dest.writeString(this.type.name());

        if (this.icon != null) {
            dest.writeString(this.icon);
        }
    }
}
