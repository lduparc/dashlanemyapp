package loicduparc.myapp.models;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by loicduparc on 28/04/15.
 */
public class ItemClickEvent {

    private View view;
    private Section section;

    public ItemClickEvent(View view, Section section) {
        this.view = view;
        this.section = section;
    }

    public View getView() {
        return view;
    }

    public Section getSection() {
        return section;
    }
}
