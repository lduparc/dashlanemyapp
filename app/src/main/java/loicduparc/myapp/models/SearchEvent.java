package loicduparc.myapp.models;

/**
 * Created by loicduparc on 28/04/15.
 */
public class SearchEvent {

    private String constraint;

    public SearchEvent(String constraint) {
        this.constraint = constraint;
    }

    public String getConstraint() {
        return constraint;
    }
}
