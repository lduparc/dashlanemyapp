// IService.aidl
package loicduparc.myapp;

import loicduparc.myapp.models.Section;
import loicduparc.myapp.listener.SectionListener;

interface IService {

    void getAllWebsites();

	void addListener(SectionListener listener);
	void removeListener(SectionListener listener);

}
