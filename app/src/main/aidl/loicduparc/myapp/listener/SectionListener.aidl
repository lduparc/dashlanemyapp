package loicduparc.myapp.listener;

import loicduparc.myapp.models.Section;

interface SectionListener {

	void handleSectionProcessed(in List<Section> sections);
	void handlehttpError(String error);
}